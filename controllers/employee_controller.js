const employees = require('../models/Employee');
let { currentId } = require('../models/Employee');

module.exports = {
  get: (req, res, next) => {

    res.send(employees.employees);

  },
  getById: (req, res, next) => {
    const { id } = req.params;
    let employee = employees.employees[id];

    if (!employee) {
      employee = `No Employee found for ID: ${id}`;
    }

    res.json(employee);

  },
  post: (req, res, next) => {
    const { employee } = req.body;

    console.log('this is req.body', req.body);

    if (!employee.name) {
      res.status(400).end('Bad Request: Employee name required');
      return;
    }

    const id = currentId++;
    
    employees.employees[id] =  {
      id,
      name: employee.name
    }

    res.status(201).json(JSON.stringify(`Successfully created: ${id}`));
  },
  delete: (req, res, next) => {
    const { id } = req.params;

    let employee = employees.employees[id];

    if (!employee) {
      employee = `No Employee found for ID: ${id}`;
      res.send(employee);
      return;
    }

    
    delete employees.employees[id];

    res.status(201).end(`Deleted Successfully: ${id}`);


  }
}