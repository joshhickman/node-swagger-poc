const fs = require('fs');

const writeSwaggerToFile = (json) => {

  fs.writeFile('swagger.json', JSON.stringify(json), (err) => {
    if (err) {
      console.log('Unable to write swagger to file');
    }

    console.log('Swagger file successfully generated');

  });
}


module.exports = writeSwaggerToFile;