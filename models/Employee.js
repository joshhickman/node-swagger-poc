// Fake employee Data //

const employees = [
  {
    id: 0,
    name: "0-index-equalizer"
  },
  {
    id: 1,
    name: 'Josh'
  },
  {
    id: 2,
    name: 'Eugene'
  },
  {
    id: 3,
    name: 'Richard'
  },
  {
    id: 4,
    name: 'Jason'
  }
];

let currentId = 5;

module.exports = {
  employees,
  currentId
}