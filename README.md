# 1. Install local dependencies
run: `npm i from root dir`

# 2. Install swagger2blueprint globally
run: `npm i -g swagger2blueprint`

# 3. Install apiary cli globally
run: `gem install apiaryio`

#4. Make sure apiary api token is exported in environment
`export APIARY_API_KEY="<token>"`





# When your ready to publish new set of docs
run : `npm run docs`

this will generate a new .apib file and push it up to the docs endpoint
[here](http://docs.inmarketemployeepoc.apiary.io/#)
