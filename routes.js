const apiController = require('./controllers/api_controller');
const employeeController = require('./controllers/employee_controller');
const router = require('express').Router();

// serves homepage
router.get('/', apiController.get);


// Swagger Definitions
// -----------------------------------------------------------

/**
 * @swagger
 * definitions:
 *  employee:
 *    type: object
 *    required:
 *      - id
 *      - name
 *    properties:
 *      id:
 *        type: integer
 *      name:
 *        type: string
 */


// Swagger Route Definitions
// ------------------------------------------------------------

/**
 * @swagger
 * /employees:
 *   get:
 *     description: Returns all current employees
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: employees
 *         headers:
 *          Content-Type:
 *            type: string
 *            default: application/json; charset=utf-8
 *         schema:
 *          type: array
 *          description: all employees
 *          items:
 *            $ref: '#/definitions/employee'     
 */
router.get('/employees', employeeController.get);

/**
 * @swagger
 * /employees/:id:
 *  get:
 *    description: Returns an employee by passed ID
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: Employee fetched successfully
 *        headers:
 *          content-type:
 *            type: string
 *            default: application/json; charset=utf-8
 *        schema:
 *          type: string
 *      404:
 *        description: Employee does not exists
 *        schema:
 *          type: string
 */
router.get('/employees/:id', employeeController.getById);

/**
 * @swagger
 * /employees:
 *  post:
 *    description: Adds an employee
 *    produces:
 *      - application/json
 *    consumes:
 *      - application/json
 *    parameters:
 *      - name: employee
 *        in: body
 *        description: the new employee to add
 *        schema:
 *          type: object
 *          required:
 *            - name
 *          properties:
 *            employee:
 *              type: object
 *              properties:
 *                name:
 *                  type: string
 *                    
 *    responses:
 *      201:
 *        description: Successfully created employee's id
 *        headers:
 *          content-type:
 *            type: string
 *            default: application/json; charset=utf-8
 *        schema:
 *          type: string
 */
router.post('/employees', employeeController.post);

/**
 * @swagger
 * /employees/:id:
 *  delete:
 *    description: Deletes an employee by ID
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: Successfully deleted employee
 *        schema:
 *          type: string
 */
router.delete('/employees/:id', employeeController.delete);


module.exports = router;

