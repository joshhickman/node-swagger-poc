import test from 'ava';
import app from './index';
import request from 'supertest';
import employeeController from './controllers/employee_controller';
import apiController from './controllers/api_controller';

// successfully returns all employees
test('get', async t => {
  const res = await request(app)
    .get('/employees')

    t.is(res.status, 200);
});

// successfully returns an employee by id
test('getById', async t => {
  const res = await request(app)
    .get('/employees/1')

    t.is(res.status, 200);
    t.is(res.body.name, "Josh");
});

// successfully posts a new user
test('post', async t => {
  const res = await request(app)
    .post('/employees')
    .send({ employee: {
      name: "TestUser123"
      }
    });

  t.is(res.status, 201);
  t.is(res.text, "Successfully created: 5");
});

// successfully errors if new user doesn't have name prop
test('post-no-name', async t => {
  const res = await request(app)
    .post('/employees')
    .send({ employee: {
      wrongKeyName: 'josh'
    } 
  });

  t.is(res.status, 400);
  t.is(res.text, "Bad Request: Employee name required");
});


//succesfully deletes a new user
test('delete', async t => {
  const res = await request(app)
    .post('/employees')
    .send({ employee: {
        name: "Josh"
      }
    });

    t.is(res.status, 201);
    t.is(res.text, "Successfully created: 6");

    const res2 = await request(app)
      .delete('/employees/6')
    
    t.is(res2.status, 201);
    t.is(res2.text, "Deleted Successfully: 6");
});
