const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const helmet = require('helmet');
const swaggerJSDoc = require('swagger-jsdoc');
const writeSwaggerToFile = require('./util/writeSwagger');

const options = {
  swaggerDefinition: {
    info: {
      title: "InMarket Interative Documentation POC",
      version: "1.0.0",
      description: "A simple node changed"
    },
    host: 'localhost:3000',
    basePath: '/',
  },
  apis: ['./routes.js'],
};

const swaggerSpec = swaggerJSDoc(options);
writeSwaggerToFile(swaggerSpec);

const router = require('./routes');

const app = express()
  .use(cors())
  .options('*', cors())
  .use(morgan('dev'))
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: true }))
  .set('view engine', 'ejs');

app.use('/', router);

/**
 * Init Server
 */
app.listen(3000, () => {
  console.log('Server running on port: 3000');
});


module.exports = app;


